import { ThemeProvider } from "./shared/contexts"
import { Header, Main } from "./shared/components"
import { useApp } from "./shared/hooks"

const App: React.FC = () => {
  const { 
    data
  } = useApp()
  
  return (
    <ThemeProvider>
      <Header />
      <Main data={data} />
    </ThemeProvider>
  )
}

export default App
