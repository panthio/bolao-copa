import { useEffect, useState } from "react"
import { formatData } from "../functions"
import { read } from "../services"
import { TData } from "../types"

export const useApp = () => {
  const [ data, setData ] = useState<TData[]>([])

  useEffect(() => {
    (async () => {
      const data = await read()
      const d = formatData(data)
      setData(d)
    })()
  }, [])

  return {
    data
  }
}
