import { Box } from '@mui/material'
import { DataGrid, GridColDef } from '@mui/x-data-grid'
import { TData } from '../../types'
import * as S from './style'

type Props = {
  data: TData[]
}

const columns: GridColDef[] = [
  { field: 'jogo', headerName: 'J', align: 'center', headerAlign: 'center', width: 1 },
  { field: 'data', headerName: 'Data', align: 'center', headerAlign: 'center' },
  { field: 'hora', headerName: 'Hora', align: 'center', headerAlign: 'center' },
  { field: 'local', headerName: 'Local', align: 'center', headerAlign: 'center', width: 200 },
  { field: 'grupo', headerName: 'Grupo', align: 'center', headerAlign: 'center' },
  { field: 'mandante', headerName: '', align: 'right', width: 130  },
  { field: 'placarMandante', headerName: '', align: 'center', width: 1, editable: true },
  { field: 'x', headerName: '', align: 'center', width: 1 },
  { field: 'placarVisitante', headerName: '', align: 'center', width: 1, editable: true },
  { field: 'visitante', headerName: '', width: 130 }
]

export const Main: React.FC<Props> = ({ data }) => {
  const rows = data.map((data, index) => ({ ...data, id: index }))
  return (
    <Box component="main" sx={S.main}>
      <DataGrid
        rows={rows}
        columns={columns}
        pageSize={6}
        autoHeight
        disableSelectionOnClick
        disableColumnFilter
        disableColumnMenu
      />
    </Box>
  )
} 
