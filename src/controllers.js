import xlsx from 'node-xlsx'
import { readFile} from 'fs/promises'

export const read = async (_, res) => {
  try {
    const filePath = 'uploads/file.xlsx'
    const file = await readFile(filePath)
    const data = xlsx.parse(file)
    return res.status(200).json(data)
  } catch (error) {
    return res.status(500).json({ message: 'Algo deu errado', error })
  }
}
