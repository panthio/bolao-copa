import express from 'express'
import { read } from './controllers.js'

const routes = express.Router()

routes.get('/', read)

export default routes
